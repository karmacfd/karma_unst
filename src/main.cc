#include <iostream>
#include "screen.h"
#include "options_parser.h"
#include "cmake_vars.h"
#include "string_utilities.h"
#include "user_input.h"
#include "volume_mesh.h"


using namespace std;
using namespace KARMA;

int main(int argc, char* argv[]) {

#ifdef KARMA_MPI
  MPI::Init(argc,argv);
#endif
#ifdef KARMA_USEPETSC 
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL); 
#ifndef KARMA_DEBUG
  PetscErrorPrintf = PetscPrintError;
#endif
#endif
#ifdef KARMA_USESLEPC
  SlepcInitialize(&argc,&argv,(char*)0," ");
#endif

  string filename;
  if (argc == 1) 
    {
      Screen::MasterError("User input filename must be specified");
    } 
  else 
    {
      filename = argv[1];
    }


string version_str = "v"+StringUtils::int2str(KARMA_VERSION_MAJOR)+"."+StringUtils::int2str(KARMA_VERSION_MINOR);
Screen::MasterInfo("Welcome to KARMA "+version_str);

#ifdef KARMA_MPI
  MPI_Barrier(MPI_COMM_WORLD);
  int mpi_rank,mpi_np;
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_np);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  Screen::MasterInfo("Running parallel on "+StringUtils::int2str(mpi_np)+" cores");
#else
  Screen::MasterInfo("Runnin serial");
#endif
  
  //read user-input 
//  UserInput userInput;
//  userInput.Init(filename);
    
  // Testing out mesh implementation
  VolumeMesh mesh;
  string mesh_fname = "grid_small.h5";
  mesh.read(mesh_fname);
//  mesh.write_wall_distance();
//  mesh.read_wall_distance();
    
    
#ifdef KARMA_MPI
  MPI::Finalize();
#endif

  return 0;
  
}
