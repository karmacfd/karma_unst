#ifndef KARMA_INPUTS_PARS_H
#define KARMA_INPUTS_PARS_H
#include <vector>
namespace KARMA {

  struct refs_user_input {
    std::vector<double> Q; 
    double density;
  };
}
#endif
