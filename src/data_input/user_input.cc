#include <iomanip>
#include <iostream>
#include <stdio.h>
#include "user_input.h"
#include "options_parser.h"

using namespace std;

namespace KARMA {
  //contructor
UserInput::UserInput() 
{
  //setting default values
  SetDefaults();
  return;
}

void UserInput::SetDefaults() 
{
  //add any default values here
  refVals.Q.resize(NVARS,0.0);
  refVals.Q[PINDEX]   = 101325.;
  refVals.Q[TINDEX]   = 300.;
}

void UserInput::UserInputRefresh()
{
  //this routine re-reads user input file
  
}

void UserInput::Init(const string &filename)
{
  //reads input file and registers sections
  filename_ = filename;

  OptionsParser options;
  options.SetFile(filename_);
  //fluid - section
  std::string sectionName="fluid";
  options.Section(sectionName);
  // reference conditions - section
  refVals.Q[PINDEX]=options.Section(sectionName).Section("referenceconditions").Get("pressure");
  refVals.Q[TINDEX]=options.Section(sectionName).Section("referenceconditions").Get("temperature");
  refVals.density  =options.Section(sectionName).Section("referenceconditions").Get("density");
  refVals.Q[UINDEX]=options.Section(sectionName).Section("referenceconditions").Get("u-velocity");
  refVals.Q[VINDEX]=options.Section(sectionName).Section("referenceconditions").Get("v-velocity");
  refVals.Q[WINDEX]=options.Section(sectionName).Section("referenceconditions").Get("w-velocity");
  // etc.

  // initial condition - section
  
  // time-integration - section
  
  
  // preconditioning - section
  
  // restart - section

  // navier stokes - section 
  

  // navier stokes ::  convective terms
  
        // flux function, scheme, limiter, order, filter

  // turbulence - section
  
  //mesh - section
  options.Section("mesh");
  options.Section("mesh").Section("sub");
  //  string mesh_filename = options.Section("mesh").Get("filename");  
  string a_string = options.Section("mesh").Get("a_string");  
  int a_int = options.Section("mesh").Get("a_int");
  double a_double = options.Section("mesh").Get("a_double");  
  //  Screen::MasterInfo("Value of a_string is "+a_string);
  //  Screen::MasterInfo("Value of a_int is "+to_string(a_int));
  //  Screen::MasterInfo("Value of a_double is "+to_string(a_double));
  

  // output - section
    
}
}
