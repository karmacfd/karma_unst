#ifndef KARMA_USER_INPUT_H
#define KARMA_USER_INPUT_H

#include "KARMA_DEFS.h"
#include "inputs_pars.h"
namespace KARMA {

class UserInput {
  //protected:
  
public:
  UserInput();
  void SetDefaults();
  void UserInputRefresh();
  void Init(const std::string &filename);
  refs_user_input  refVals;
  std::string filename_;
};
}
#endif

