#include "volume_mesh.h"
#include "string_utilities.h"
#include <set>

using namespace std;

namespace KARMA {

void VolumeMesh::create_faces_and_nodes(bool read_coordinates) {

    // Clean-up
    node.resize(0);
    face.resize(0);
    num_nodes = 0;
    num_faces = 0;
    node_gid2local.clear();
    face_gid2local.clear();

    for (int c=0;c<cell.size();++c) {
        cell[c].nodes.resize(0);
        cell[c].faces.resize(0);
    }

    // Read chunks of face left and right neighbor list, grab those that belong to the current proc
    hid_t file,volume,attr;

    file   = H5Fopen(file_name_.c_str(),H5F_ACC_RDONLY,H5P_DEFAULT);
    volume = H5Gopen(file,"/volume",H5P_DEFAULT);

    vector<vector<int> > cell_faces;
    cell_faces.resize(num_cells);

    // Read a global_num_faces/mpi_np_ sized chunk from face left and right 
    // neighbor lists in the mesh file
    map<int,int>::iterator it1,it2;
    std::vector<bool> flip_face_nodes;
    
    Screen::MasterInfo("Reading face neighbors and creating faces",1); 
    for (int p=0;p<mpi_np_;++p) {
        
        int face_count  = floor(double(global_num_faces)/double(mpi_np_));
        int face_offset = p*face_count;
        if (p==mpi_np_-1) face_count = face_count+global_num_faces-mpi_np_*face_count;
        flip_face_nodes.resize(face_count);

        hsize_t h5_offset[1];
        hsize_t h5_count[1];
        h5_offset[0] = face_offset;
        h5_count[0]  = face_count;
        hid_t dataset, dataspace, memspace;
        
        vector<int> left_chunk(face_count,0);
        vector<int> right_chunk(face_count,0);
        
        dataset   = H5Dopen(file, "/volume/face_left_neighbor", H5P_DEFAULT);
        dataspace = H5Dget_space(dataset); 
        H5Sselect_hyperslab(dataspace,H5S_SELECT_SET,h5_offset,NULL,h5_count,NULL);
        memspace  = H5Screate_simple(1,h5_count,NULL);
        
        H5Dread(dataset,H5T_NATIVE_INT,memspace,dataspace,H5P_DEFAULT,&left_chunk[0]);
        H5Dclose(dataset);
        H5Sclose(dataspace);
        H5Sclose(memspace);
        
        dataset   = H5Dopen(file, "/volume/face_right_neighbor", H5P_DEFAULT);
        dataspace = H5Dget_space(dataset); 
        H5Sselect_hyperslab(dataspace,H5S_SELECT_SET,h5_offset,NULL,h5_count,NULL);
        memspace  = H5Screate_simple(1,h5_count,NULL);
        
        H5Dread(dataset,H5T_NATIVE_INT,memspace,dataspace,H5P_DEFAULT,&right_chunk[0]);
        H5Dclose(dataset);
        H5Sclose(dataspace);
        H5Sclose(memspace);

        // Loop the read faces
        for (int f=0;f<face_count;++f) {
            it1=cell_gid2local.find(left_chunk[f]);
            it2=cell_gid2local.find(right_chunk[f]);
            bool own_left  = (it1!=cell_gid2local.end());
            bool own_right = (it2!=cell_gid2local.end());
            
            // If the current partition owns either the left or the right cell 
            // neighbor of the face
            if (own_left || own_right ) { // A face will be created
                int leftc  = (*it1).second;
                int rightc = (*it2).second;
                // Create the face and insert it to cell faces list
                face.resize(face.size()+1);
                VolumeFace &this_face=face[face.size()-1];
                this_face.gid = face_offset+f;

                if (own_left) {
                    flip_face_nodes[f] = false;
                    this_face.parent   = leftc;
//DEBUG cout << "leftc " << leftc << endl;
                    if (!own_right) {
                        // If I own the left cell but not the right cell,
                        // This face must either be a bc face or a partition interface face
                        if (right_chunk[f]<0) { // BC face
                            this_face.type = BOUNDARY_FACE;
                            this_face.bc   = -right_chunk[f]-1;
                        } else {
                            this_face.type     = PARTITION_FACE;
                            this_face.neighbor = right_chunk[f]; // Set to global id
                        }
                    } else {
                        // I own both left and right cells, the face is internal
                        this_face.type     = INTERNAL_FACE;
                        this_face.neighbor = rightc; // Set to local id
                    }
                } else { 
                    // If I own the right cell only, it should be a partition face
                    // Need to reorient the face nodes so that the normal points 
                    // outwards from the parent cell.
                    flip_face_nodes[f] = true;
                    this_face.parent   = rightc;
                    if (left_chunk[f]<0) { // BC face
                        Screen::MasterError("[VolumeMesh::create_faces_and_nodes] unexpected value when reading face connectivity");
                    } else {
                        this_face.type     = PARTITION_FACE;
                        this_face.neighbor = left_chunk[f]; // Set to global id
                    }
                }

                face_gid2local.insert(pair<int,int>(this_face.gid,face.size()-1));

            } // if face is in current proc
        } // face count loop 

        left_chunk.clear();
        right_chunk.clear();

    } // proc loop
    
    vector<bool>       (flip_face_nodes).swap(flip_face_nodes);
    vector<VolumeFace> (face).swap(face);
    
    // Update the number of faces
    num_faces = face.size();

    // Read face node connectivity from hdf5 file
    // First read the face connectivity list
    // Read the connectivity list of the faces and the next faces (denoted as plus)
    // in order to determine the connectivity list size
    Screen::MasterInfo("Reading face connectivity",1); 
    
    vector<int>     face_conn_index      (num_faces);
    vector<hsize_t> h5indices            (num_faces);
    vector<int>     face_conn_index_plus (num_faces);
    vector<hsize_t> h5indices_plus       (num_faces);
    hsize_t count[1];
    count[0] = num_faces;
    
    for (int f=0;f<num_faces;++f) {
        h5indices[f]=face[f].gid;
        if (face[f].gid<(global_num_faces-1)) h5indices_plus[f] = face[f].gid+1;
        else                                  h5indices_plus[f] = face[f].gid;
    }

    hid_t dataset   = H5Dopen(file,"/volume/face_connectivity_index",H5P_DEFAULT);
    hid_t dataspace = H5Dget_space(dataset); 
    hid_t memspace  = H5Screate_simple(1,count,NULL);
    H5Sselect_elements(dataspace,H5S_SELECT_SET,num_faces,(const hsize_t *) &h5indices[0]);
    H5Dread(dataset,H5T_NATIVE_INT,memspace,dataspace,H5P_DEFAULT,&face_conn_index[0]);
    H5Sclose(memspace);
    H5Sclose(dataspace);
    h5indices.clear();

    dataspace = H5Dget_space(dataset); 
    memspace  = H5Screate_simple(1,count,NULL);    
    H5Sselect_elements(dataspace,H5S_SELECT_SET,num_faces,(const hsize_t *) &h5indices_plus[0]);

    H5Dread(dataset,H5T_NATIVE_INT,memspace,dataspace,H5P_DEFAULT,&face_conn_index_plus[0]);
    H5Sclose(memspace);
    H5Sclose(dataspace);
    h5indices_plus.clear();
    H5Dclose(dataset);

    // Get face connectivity array size (global size)
    dataset   = H5Dopen(file, "/volume/face_connectivity", H5P_DEFAULT);
    dataspace = H5Dget_space(dataset); 
    hsize_t face_conn_size[1];
    face_conn_size[0] = H5Sget_simple_extent_npoints(dataspace);

    // The face connectivity indices read from the hdf5 file were on global indexing
    // Convert face_conn_index to local indexing
    // Assuming the nodes are created in the order they show up in the face connectivity list
    vector<int> face_conn_index_local(num_faces);
    int conn_count       = 0;
    int total_conn_count = 0;
    face_conn_index_local[0]=0;
    for (int f=1;f<num_faces;++f) {
        // If it is the last face
        if (face[f-1].gid==(global_num_faces-1)) conn_count = face_conn_size[0]-face_conn_index[f-1]; 
        else                                     conn_count = face_conn_index_plus[f-1]-face_conn_index[f-1];
        face_conn_index_local[f] = face_conn_index_local[f-1]+conn_count;
        total_conn_count          += conn_count;
    }
    if (face[num_faces-1].gid==(global_num_faces-1)) conn_count = face_conn_size[0]-face_conn_index[num_faces-1]; 
    else                                             conn_count = face_conn_index_plus[num_faces-1]-face_conn_index[num_faces-1];
    total_conn_count += conn_count;

    face_conn_index_plus.clear();
    h5indices.clear();
    h5indices.reserve(total_conn_count);
    
    for (int f=0;f<num_faces;++f) {
        if (f==(num_faces-1)) conn_count = total_conn_count-face_conn_index_local[f]; 
        else                  conn_count = face_conn_index_local[f+1]-face_conn_index_local[f];
        for (int i=face_conn_index[f];i<face_conn_index[f]+conn_count;++i) {
            h5indices.push_back(i);
        }
    }

    // Overwrite face_conn_index with local version
    face_conn_index=face_conn_index_local;
    face_conn_index_local.clear();
    face_conn_size[0]=total_conn_count;
    
    
    // Read the actual face connectivity list
    H5Sselect_elements(dataspace,H5S_SELECT_SET,total_conn_count,(const hsize_t *) &h5indices[0]);
    memspace=H5Screate_simple(1,face_conn_size,NULL);
    h5indices.clear();
    vector<int> face_connectivity(face_conn_size[0]);
    H5Dread(dataset,H5T_NATIVE_INT,memspace,dataspace,H5P_DEFAULT,&face_connectivity[0]);
    H5Sclose(memspace);
    H5Sclose(dataspace);
    H5Dclose(dataset);

    // Insert entries of face_connectivity into a set to get the unique ones
    set<int> my_nodes;
    for (int i=0;i<face_connectivity.size();++i) my_nodes.insert(face_connectivity[i]);
    num_nodes = my_nodes.size();

    // Create the nodes
    Screen::MasterInfo("Creating nodes",1); 
    node.resize(num_nodes);
    hsize_t h5node_indices[num_nodes][2];
    vector<double> coord(num_nodes);
    int counter=0;
    for (set<int>::iterator it=my_nodes.begin();it!=my_nodes.end();++it) {
        h5node_indices[counter][0]=*it;
        h5node_indices[counter][1]=0;
        node[counter].gid=*it;
        node_gid2local.insert(pair<int,int>(*it,counter));
        counter++;
    }
    vector<VolumeNode> (node).swap(node);
    
    // Read the node coordinates from the hdf5 file
    if (read_coordinates) {
        Screen::MasterInfo("Reading node x-coordinates",1);
        dataset=H5Dopen(file,"/volume/nodes",H5P_DEFAULT);
        dataspace=H5Dget_space(dataset);
        count[0]=num_nodes;
        memspace=H5Screate_simple(1,count,NULL);
        H5Sselect_elements(dataspace,H5S_SELECT_SET,num_nodes,(const hsize_t *) h5node_indices);
        H5Dread(dataset,H5T_NATIVE_DOUBLE,memspace,dataspace,H5P_DEFAULT,&coord[0]);
        for (int n=0;n<num_nodes;++n) node[n][0]=coord[n];

        Screen::MasterInfo("Reading node y-coordinates",1);
        for (int n=0;n<num_nodes;++n) h5node_indices[n][1]=1;
        H5Sselect_elements(dataspace,H5S_SELECT_SET,num_nodes,(const hsize_t *) h5node_indices);
        H5Dread(dataset,H5T_NATIVE_DOUBLE,memspace,dataspace,H5P_DEFAULT,&coord[0]);
        for (int n=0;n<num_nodes;++n) node[n][1]=coord[n];

        Screen::MasterInfo("Reading node z-coordinates",1);
        for (int n=0;n<num_nodes;++n) h5node_indices[n][1]=2;
        H5Sselect_elements(dataspace,H5S_SELECT_SET,num_nodes,(const hsize_t *) h5node_indices);
        H5Dread(dataset,H5T_NATIVE_DOUBLE,memspace,dataspace,H5P_DEFAULT,&coord[0]);
        for (int n=0;n<num_nodes;++n) node[n][2]=coord[n];

        H5Sclose(memspace);
        H5Sclose(dataspace);
        H5Dclose(dataset);
    }

    Screen::MasterInfo("Populating face->nodes",1);
    for (int f=0;f<num_faces;++f) {
        int node_count;
        if (f==(num_faces-1)) node_count = face_connectivity.size()-face_conn_index[f];
        else                  node_count = face_conn_index[f+1]-face_conn_index[f];
        face[f].nodes.resize(node_count);
        for (int fn=0;fn<node_count;++fn) {
            face[f].nodes[fn]=node_gid2local[face_connectivity[face_conn_index[f]+fn]];
        }
        if (flip_face_nodes[f]) {
            vector<int>::reverse_iterator rit;
            vector<int> temp=face[f].nodes;
            face[f].nodes.assign(temp.rbegin(),temp.rend());
        }
        // Check for repeated nodes
        for (int fn1=0;fn1<face[f].nodes.size();++fn1) {
            int n1=face[f].nodes[fn1];
            for (int fn2=fn1+1;fn2<face[f].nodes.size();++fn2) {
                int n2=face[f].nodes[fn2];
                if (n1==n2) {
                    Screen::MasterWarning("Repeated nodes are found in face "+StringUtils::int2str(f)+" with gid "+StringUtils::int2str(face[f].gid));
                }
            }
        }
    }

    flip_face_nodes.clear();
    face_conn_index.clear();
    face_connectivity.clear();

    Screen::MasterInfo("Populating cell->faces",1);
    for (int f=0;f<num_faces;++f) {
        cell[face[f].parent].faces.push_back(f);	
        if (face[f].type==INTERNAL_FACE) cell[face[f].neighbor].faces.push_back(f);	
    }
    // Trim extra memory due to push_back
    for (int c=0;c<num_cells;++c) vector<int> (cell[c].faces).swap(cell[c].faces);

    Screen::MasterInfo("Populating cell->nodes",1);
    for (int c=0;c<num_cells;++c) {
        set<int> node_set;
        set<int>::iterator it;
        for (int cf=0;cf<cell[c].faces.size();++cf) {
            int f=cell[c].faces[cf];
            for (int fn=0;fn<face[f].nodes.size();++fn) {
                node_set.insert(face[f].nodes[fn]);
            }
        }
        for (set<int>::iterator it=node_set.begin();it!=node_set.end();++it) {
            cell[c].nodes.push_back(*it);
        }
        node_set.clear();
        vector<int> (cell[c].nodes).swap(cell[c].nodes);
    }

    Screen::MasterInfo("Populating node->cells",1);
    bool flag;
    for (int c=0;c<num_cells;++c) {
        int n;
        for (int cn=0;cn<cell[c].nodes.size();++cn) {
            n=cell[c].nodes[cn];
            flag=false;
            for (int i=0;i<node[n].cells.size();++i) {
                if (node[n].cells[i]==c) {
                    flag=true;
                    break;
                }
            }
            if (!flag) {
                node[n].cells.push_back(c);
            }
        }
    }
    for (int n=0;n<num_nodes;++n) vector<int> (node[n].cells).swap(node[n].cells);

    H5Gclose(volume);
    H5Fclose(file);

    return;
}
    
} // end namespace KARMA