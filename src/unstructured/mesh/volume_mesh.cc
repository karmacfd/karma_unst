#include "volume_mesh.h"

using namespace std;

namespace KARMA {

VolumeMesh::VolumeMesh () {
    
    // Initialize everything
    mpi_rank_ = 0;
    mpi_np_   = 1;
#ifdef KARMA_MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank_);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_np_);
#endif
    file_name_ = "";
    
    global_num_cells = 0;
    global_num_faces = 0;
    global_num_nodes = 0;
    mpi_cell_offset  = 0;
    num_cells = 0;
    num_faces = 0;
    num_nodes = 0;
    bc_count  = 0; 

    return;
}

VolumeNode::VolumeNode(double x, double y, double z) {
    comp[0]=x;
    comp[1]=y;
    comp[2]=z;
    gid    =VALUE_UNASSIGNED;
}

VolumeFace::VolumeFace () {
    // Initialize everything
    gid      = VALUE_UNASSIGNED;
    parent   = VALUE_UNASSIGNED;
    neighbor = VALUE_UNASSIGNED;
    area     = 0.;
    normal   = 0.;
    centroid = 0.;
    bc       = VALUE_UNASSIGNED;
    type     = VALUE_UNASSIGNED;
    return;
}

VolumeCell::VolumeCell () {
    // Initialize everything
    gid = VALUE_UNASSIGNED;
    wall_distance = 1.e20;
    return;
}

void VolumeMesh::write_wall_distance () {

    // Collect data in an array
    vector<double> wdist(num_cells,0.);
    for (int c=0;c<num_cells;++c) wdist[c] = cell[c].wall_distance;
    
    hid_t file,volume,dataspace,dataset,memspace;
    hsize_t datasize;
    
    string out_file_name = "closest_wall_distance.h5";
    string var_name      = "closest_wall_distance";

    file      = H5Fcreate(out_file_name.c_str(),H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
    volume    = H5Gcreate(file,"volume",H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    datasize  = global_num_cells;
    dataspace = H5Screate_simple(1,&datasize,NULL);
    dataset   = H5Dcreate(volume,var_name.c_str(),H5T_NATIVE_DOUBLE,dataspace,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

    vector<hsize_t> h5indices(num_cells);
    hsize_t count[1];
    count[0] = num_cells;
    for (int c=0;c<num_cells;++c) h5indices[c]=cell[c].gid;

    memspace  = H5Screate_simple(1,count,NULL);
    H5Sselect_elements(dataspace,H5S_SELECT_SET,num_cells,(const hsize_t *) &h5indices[0]);
    
    H5Dwrite(dataset,H5T_NATIVE_DOUBLE,memspace,dataspace,H5P_DEFAULT,&wdist[0]);
    
    H5Sclose(memspace);
    H5Sclose(dataspace);
    H5Dclose(dataset);
    H5Gclose(volume);
    H5Fclose(file);
    
    return;
}

bool VolumeMesh::read_wall_distance () {
    
    string in_file_name = "closest_wall_distance.h5";
    string var_name     = "closest_wall_distance";
    
    if (!File_IO::FileExists(in_file_name)) return false;

    vector<double> wdist(num_cells,-1.);
    
    hid_t file,volume,dataset,dataspace,memspace;

    file      = H5Fopen(in_file_name.c_str(),H5F_ACC_RDONLY,H5P_DEFAULT);
    volume    = H5Gopen(file,"volume",H5P_DEFAULT);
    dataset   = H5Dopen(volume,var_name.c_str(),H5P_DEFAULT);
    dataspace = H5Dget_space(dataset); 

    vector<hsize_t> h5indices(num_cells);
    hsize_t count[1];
    count[0] = num_cells;
    for (int c=0;c<num_cells;++c) h5indices[c]=cell[c].gid;

    memspace  = H5Screate_simple(1,count,NULL);
    H5Sselect_elements(dataspace,H5S_SELECT_SET,num_cells,(const hsize_t *) &h5indices[0]);
    
    H5Dread(dataset,H5T_NATIVE_DOUBLE,memspace,dataspace,H5P_DEFAULT,&wdist[0]);

    H5Sclose(memspace);
    H5Sclose(dataspace);
    H5Dclose(dataset);
    H5Gclose(volume);
    H5Fclose(file);
    
    for (int c=0;c<num_cells;++c) cell[c].wall_distance = wdist[c];
    
    return true;
}

} // end namespace KARMA