#include "volume_mesh.h"
#include "string_utilities.h"

#include "parmetis.h"
#include <limits>

using namespace std;

namespace KARMA {

void VolumeMesh::read (string &file_name) {
    file_name_ = file_name; 
    // Check if file exists  
    Screen::MasterInfo("Reading the unstructured mesh from "+file_name_);
    if (mpi_rank_==0) File_IO::AssertFileExists(file_name_);
    read_header();
    dummy_partition();
    parmetis_partition();
    
    return;    
}

void VolumeMesh::read_header () {

    hid_t file,volume,attr;

    file   = H5Fopen(file_name_.c_str(),H5F_ACC_RDONLY,H5P_DEFAULT);    
    volume = H5Gopen(file,"volume",H5P_DEFAULT);
    
    attr   = H5Aopen(volume,"number_of_cells",H5P_DEFAULT);
    H5Aread(attr,H5T_NATIVE_INT,&global_num_cells);
    H5Aclose(attr);
    Screen::MasterInfo("Global number of cells = "+StringUtils::int2str(global_num_cells),1);

    attr   = H5Aopen(volume,"number_of_faces",H5P_DEFAULT);
    H5Aread(attr,H5T_NATIVE_INT,&global_num_faces);
    H5Aclose(attr);
    Screen::MasterInfo("Global number of faces = "+StringUtils::int2str(global_num_faces),1);
    
    attr   = H5Aopen(volume,"number_of_nodes",H5P_DEFAULT);
    H5Aread(attr,H5T_NATIVE_INT,&global_num_nodes);
    H5Aclose(attr);
    Screen::MasterInfo("Global number of nodes = "+StringUtils::int2str(global_num_nodes),1);

    Screen::MasterInfo("Boundary regions:",1);
    attr=H5Aopen(volume,"BC_names",H5P_DEFAULT);                                
    hid_t memtype = H5Tcopy(H5T_C_S1);                                            
    hid_t space   = H5Aget_space(attr);                                             
    hsize_t dims[1];                   
    H5Sget_simple_extent_dims(space,dims,NULL);                                 
    H5Tset_size(memtype,H5T_VARIABLE);                                          
    bc_names.resize(int(dims[0]));                                               
    bc_count      = bc_names.size();                                                     
    char **rdata = (char **) malloc (int(dims[0]*sizeof(char *)));    
    H5Aread(attr,memtype,rdata);                                                
    for (int i=0;i<dims[0];++i) {                                               
        bc_names[i]=rdata[i];                                                    
        Screen::MasterInfo("BC_"+StringUtils::int2str(i+1)+" : "+bc_names[i],2);               
    }                                                                           
    H5Aclose(attr); 

    return;  
};
    
void VolumeMesh::dummy_partition () {
    
    Screen::MasterInfo("Performing dummy partitioning before ParMetis");
    
    // Partition the mesh simply by splitting the list of cells into n pieces
    // This manual partitioning is done to be able to use parmetis
    num_cells = floor(double(global_num_cells)/double(mpi_np_));
  
    // Cell counts from each proc           
    vector<int> partition_cell_counts(mpi_np_,num_cells);
    // If the last proc
    if (mpi_rank_==mpi_np_-1) {
        num_cells = global_num_cells-mpi_rank_*num_cells;
        partition_cell_counts[mpi_rank_] = num_cells;
    }
    
    // Create the cells
    cell.resize(num_cells);
    
    mpi_partition_cell_offsets.resize(mpi_np_);
    mpi_partition_cell_offsets[0]=0;                                                       
    for (int p=1;p<mpi_np_;++p) mpi_partition_cell_offsets[p]=mpi_partition_cell_offsets[p-1]+partition_cell_counts[p-1];               
    mpi_cell_offset = mpi_partition_cell_offsets[mpi_rank_];                                                                        
                                                                                                           
    // Set cell global ID's
    for (int c=0;c<num_cells;++c) {
        cell[c].gid = mpi_cell_offset+c;
        cell_gid2local.insert(pair<int,int> (cell[c].gid,c));
    }
    
    create_faces_and_nodes(false); // Don't read the coordinates yet 
    
    return;
}
    
void VolumeMesh::parmetis_partition() {

#ifdef KARMA_MPI

    Screen::MasterInfo("Performing the actual mesh partitioning");
    // Clean-up
    cell_gid2local.clear();

    // Parmetis usage
    //int ParMETIS_V3_PartMeshKway (
    // idx t *elmdist, idx t *eptr, idx t *eind, idx t *elmwgt, idx t *wgtflag, idx t *numflag,
    // idx t *ncon, idx t *ncommonnodes, idx t *nparts, real t *tpwgts, real t *ubvec,
    // idx t *options, idx t *edgecut, idx t *part, MPI Comm *comm)
    
    // ParMETIS_V3_PartMeshKway(idx_t *elmdist, idx_t *eptr, idx_t *eind, idx_t *elmwgt, int *wgtflag, int *numflag, int *ncon, int * ncommonnodes, int *nparts, float *tpwgts, float *ubvec, int *options, int *edgecut, idx_t *part, MPI_Comm) 
    // elmdist      : Range of cells (offsets) in each processor. Sized (np+1) 
    // eptr         : Cell connectivity indices
    // eind         : Cell connectivity list
    // elmwgt       : Element weights [null]
    // wgtflag      : Can take value of 0,1,2,3 [0: no weights)
    // numflag      : 0 C-style numbers, 1 Fortran-style numbers
    // ncon         : Number of weights each cell has [1]
    // ncommonnodes : An edge is created between cells sharing at least this many nodes
    // nparts       : Number of partitions requested
    // tpwgts       : Size ncon x nparts. Function of weights for each part (set all to 1/np)
    // ubvec        : Balance tolerance, 1.05 is recommended
    // options      : [0 1 15] for default
    // edgecut      : OUTPUT, number of edges
    // part         : OUTPUT, sized local cell count. Which partition each local cell goes to
    // comm         : MPI communicator

    // Range of cells in each processor

    idx_t elmdist[mpi_np_+1]; 
    for (int p=0;p<mpi_np_;++p) elmdist[p]=mpi_partition_cell_offsets[p];
    elmdist[mpi_np_]=global_num_cells;

    // Fill in eptr and eind
    idx_t *eptr;
    eptr = new idx_t[num_cells+1]; 
    eptr[0]=0;
    for (int c=1;c<=num_cells;++c) eptr[c]=eptr[c-1]+cell[c-1].nodes.size();
    idx_t *eind;
    eind = new idx_t[eptr[num_cells]];
    int counter=0;
    for (int c=0;c<num_cells;++c) {
        for (int cn=0;cn<cell[c].nodes.size();++cn) {
            eind[counter]=node[cell[c].nodes[cn]].gid;
            counter++;
        }
    }
    int ncommonnodes=3; // number of shared nodes to be considered an edge in the graph

    idx_t* part = new idx_t[num_cells];

    // Rest of parmetis inputs
    idx_t* elmwgt = NULL;
    int wgtflag = 0; // no weights associated with elem or edges
    int numflag = 0; // C-style numbering
    int ncon    = 1; // # of weights or constraints

    float tpwgts[mpi_np_];
    for (int p=0; p<mpi_np_; ++p) tpwgts[p]=1./float(mpi_np_);
    float ubvec=1.01; // Allow up to 1% imbalance
    int options[3]; // default values for timing info set 0 -> 1
    options[0]=0; options[1]=1; options[2]=15;
    int edgecut ; // output

    Screen::MasterInfo("Calling Parmetis",1);
    MPI_Comm commWorld=MPI_COMM_WORLD;
    ParMETIS_V3_PartMeshKway(elmdist,eptr,eind, elmwgt,
            &wgtflag, &numflag, &ncon, &ncommonnodes,
            &mpi_np_, tpwgts, &ubvec, options, &edgecut,
            part,&commWorld) ;
    Screen::MasterInfo("Partitioning is done",1);
    delete[] eptr;
    delete[] eind;

    vector<int> nsend (mpi_np_,0);
    vector<int> nrecv (mpi_np_,0); 
    vector<vector<int> > send(mpi_np_);

    for (int c=0;c<num_cells;++c) {
        nsend[part[c]]++;
        send[part[c]].push_back(cell[c].gid);
    }
    MPI_Alltoall(&nsend[0],1,MPI_INT,&nrecv[0],1,MPI_INT,MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);

    cell.resize(0);

    for (int p=0;p<mpi_np_;++p) {
        vector<int> recv(nrecv[p],-1);

        MPI_Sendrecv(&send[p][0],nsend[p],MPI_INT,p,0,
                &recv[0],nrecv[p],MPI_INT,p,0,
                MPI_COMM_WORLD,MPI_STATUS_IGNORE);

        // Create the cells
        for (int c=0;c<recv.size();++c) {
            VolumeCell temp;
            temp.gid=recv[c];
            cell_gid2local.insert(pair<int,int> (recv[c],cell.size()));
            cell.push_back(temp);
        }
        recv.clear();
    }
    send.clear();

    // Trim padding due to push_back
    vector<VolumeCell> (cell).swap(cell);

    num_cells=cell.size();
    vector<int> partition_cell_counts(mpi_np_,0);
    MPI_Allgather(&num_cells,1,MPI_INT,&partition_cell_counts[0],1,MPI_INT,MPI_COMM_WORLD);

    if (mpi_rank_==0) {
        // Calculate cell count imbalance
        int max_cell_count=0;
        int min_cell_count=numeric_limits<int>::max();
        for (int i=0;i<mpi_np_;++i) {
            max_cell_count=max(max_cell_count,partition_cell_counts[i]);
            min_cell_count=min(min_cell_count,partition_cell_counts[i]);
        }
        double target=double(global_num_cells)/double(mpi_np_);
        double imb=fabs(target-double(max_cell_count))/target;
        Screen::MasterInfo("Partitioning summary:",1);
        Screen::MasterInfo("Cell count balance = "+to_string((1.-imb)*100.)+"%",2);
        Screen::MasterInfo("Number of edge cuts  = "+to_string(edgecut),2);
    }

    mpi_partition_cell_offsets.resize(mpi_np_);
    mpi_partition_cell_offsets[0]=0;                                                       
    for (int p=1;p<mpi_np_;++p) mpi_partition_cell_offsets[p]=mpi_partition_cell_offsets[p-1]+partition_cell_counts[p-1];               
    mpi_cell_offset = mpi_partition_cell_offsets[mpi_rank_];   

    delete[] part;
#endif

    create_faces_and_nodes();
    return;
}

} // end namespace KARMA