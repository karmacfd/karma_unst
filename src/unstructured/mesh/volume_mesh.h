#ifndef KARMA_VOLUME_MESH_H
#define KARMA_VOLUME_MESH_H

#include "screen.h"
#include "file_io.h"
#include "vec3d.h"
#include "hdf5.h"

#define VALUE_UNASSIGNED -99

// Face type numbering
#define INTERNAL_FACE     1
#define PARTITION_FACE    2
#define BOUNDARY_FACE     3


namespace KARMA {
    
class VolumeNode : public Vec3D {
    public:
        int gid; // Global id
        std::vector<int> cells; // Local ID of cells neighboring this node
        VolumeNode (double x=0., double y=0., double z=0.); // Constructor
};
    
class VolumeFace {
    public:
        int parent,neighbor,type,bc,gid;
        std::vector<int> nodes;
        double area;
        Vec3D normal; // Normal points away from the parent cell into the neighbor cell
        Vec3D centroid;
        VolumeFace(); // Constructor
};

class VolumeCell {
    public:
        std::vector<int> nodes,faces;
        int gid; // Global id
        double wall_distance;
        VolumeCell(); // Constructor
};

class VolumeMesh {
    private:
        int mpi_rank_,mpi_np_;
        std::string file_name_;
    public:
        std::vector<VolumeCell> cell;
        std::vector<VolumeFace> face;
        std::vector<VolumeNode> node;
        
        int global_num_cells,global_num_faces,global_num_nodes;
        int mpi_cell_offset; // Sum of number of cells in all lower ranked procs
        std::vector<int> mpi_partition_cell_offsets;
        int num_cells,num_faces,num_nodes;
        int bc_count;
        std::vector<std::string> bc_names;
        
        // Mappings from global ids to local ids
        std::map<int,int> node_gid2local;
        std::map<int,int> cell_gid2local;
        std::map<int,int> face_gid2local;
        
        VolumeMesh(); // Constructor
        
        void read (std::string &file_name);
        void read_header();
        void dummy_partition();
        void parmetis_partition();
        void create_faces_and_nodes(bool read_coordinates=true);
        
        void write_wall_distance();
        bool read_wall_distance();
};
    
} // end namespace KARMA

#endif